**Reunião Checklist - 30/11/2023** 

- [x] Update the boards (move the cards) 
- [x] Recording time spent  
- [x] Check/update risk plan 
- [x] Code + Unit tests  
- [x] Acceptance Criteria for the finished US 
- [x] Code review if you created merge requests from 'dev' into 'qa' 
- [x] Pipeline 
    - [x] run unit tests for every commit 
    - [x] create and run jar //Deu erro a fazer o ficheiro jar
For the groups finishing Sprint 2: 
    - Sprint 2 review: 
        - [x] Demo prepared (in qa branch, with data prepared. Compare with goal) //Deu erro a fazer o ficheiro jar 
        - [x] Collect client feedback 
        - [x] Compare estimates (SP and Hours) vs real 
        - [x] Show DoD 
    - Sprint 3 plan: 
        - [x] Sprint Goal 
        - [x] US+Estimates 

    For groups going to the Sprint 3 progress meeting: 
    - [x] Sprint 2 review meeting report 
    - [x] Sprint 2 retrospective meeting report 
    - [x] Tasks list + estimates 
    - [x] AC for every US 


**Reunião Checklist - 06/12/2023** 

| Week 12: progress meeting, during sprint 3 | Points (0-3) | Comments | 
| ------ | ------ | ------ | 
|Sprint 3 Backlog board has Tasks and AC cards with estimates/real |3| |
|Product Backlog board has US cards selected for current sprint |3| |
|Code and unit tests are finished, according to cards |2| |
|Run unit tests automatically at every commit |3| |
|Merge request (from dev into qa) at the end of every task. Code was reviewed |2| |
|Acceptance test run at the end of every US |3| |
|Create and run deployed code |3| |
|Risk plan is updated |3| |
|Control project - compare estimates (SP and Hours) vs real |3| |
|Sprint 2 Review meeting report is updated |3| |
|Sprint 2 Retrospective meeting report is updated |3| |
|Sprint 3 plan has Sprint Goal and Selected US/Tasks/AC with estimates |3| |
