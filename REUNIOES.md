# Feedback Reuniões
## Reunião 1 - 12/10/2023 ás 15h25:
- Adicionar o email aos nomes no readme ✅
- Tirar do project background a solução ✅
- Reiterar a lista dos users para incluir apenas a organização, voluntários e doadores; ✅
- Elaborar o vision statement para explicar quais as exatas funcionalidades da aplicação e com que fim estão a ser feitas (+ potencialmente incluir um desenho que explica a linha de pensamento de cada funcionalidade: ex. ligação entre um voluntário e um animal que explica o que cada um faz) ✅
- Alterações ao que planeamos fazer ou não; ✅
- Discutir o ponto sobre adoção dos animais por um voluntário; ✅
- Atualizar a user story 3 para apenas conter uma user story; ✅
- Criar um caso de uso generalizado para várias user stories que possam falar sobre um mesmo user; ✅
- Alterações ao risk plan para alterar o thresshold of sucess de modo a ser alinhado com os riscos; ✅

- Alterações à list of features: ✅
> - lista e informações de cada animal (raça, onde se encontra, etc.);
> - lista e informações sobre cada voluntário e de que animal é que trata;
> - os voluntários devem ser separados em categorias (tratadores, participam em campanhas, participam com mão de obra na construção de casas para os animais ou fazem limpeza, etc);
> - lista de doadores (quem adota um animal e quem doa dinheiro pertencem à mesma lista de doadores);
> - lista de veterinários para caso haja a necessidade de chamar algum ao local ter o contacto do mesmo
> - eventualmente adicionar a possibilidade de atribuir categorias a apenas alguns voluntários para editarem informações dos animais;

- Para a 1ª release:
> - estado de adoção do animal;
> - gestão de atividades dos animais (se já comeram, estado de saúde) com informações visuais como bolas de diferentes cores que representem o seu estado atual;
> - gestão das boxes dos animais;

- Para a release final:
> - gestão dos veterinários onde é fornecida uma lista com informação sobre os animais mais completa do que aquela na aba dos animais apresentada para os restantes usuários;

## Reunião 2 - 19/10/2023 ás 15h15:
- Remover Animais dos Stakeholders; ✅
- Remover a primeira frase do Vision statement; ✅
- Simplificar/Melhorar o Vision statement; (gerir os animais, gerir os voluntários, gerir as instalações, gerir o material, gerir doadores); ✅
- Adicionar um campo para o estado de saude do animal e um espaço para comentários; ✅
- Fazer uma lista mais simplificada da listas todas (mockups) e sim posteriormente mostrar uma informação mais complexa; 
- Procurar o animal pelo nome, e por outros critérios (tipo animal, local onde se encontra); ✅
- Subsituir todos os campos onde estão organização por administrador; ✅
- Criar uma label para cada US (US1, US2, US3, etc); ✅
- Melhorar os criterios de aceitação (simplificar), tentar explicitar melhor os campos como por exemplo informações pessoais (nome, morada, telemovel); ✅
- Rever as user story; ✅
- Acresentar pelo menos +3 Threshold of Sucess; ✅
- Melhorar o Risk List, (Devido à falta de experiência da equipa com projetos deste tipo, pode levar a falhar a conclusão das metas autropropostas); ✅
- Melhorar a Mitigation Actions que temos (caso não consigamos comprir o prazo presuposto das metas autopropostas, falar com o cliente para negociar melhor as US a serem desenvolvidas); ✅
- Adicionar no Goal do Sprint 0 Plan, as boards tambem; ✅
- Mudar o nome das US; ✅
- Remover as open e close dos boards; ✅
- Editar section do product backlog; ✅
- Release Plan (dizer o que deve estar pronto, sem estar ligado a programação, exemplo: Toda a parte da gestão das instalações, etc); ✅
- Definir metas para a sprint 0 (US, a serem apresentadas) ✅

## Reunião 3 - 26/10/2023 ás 15h50:
- Threshold of Sucess tirar os 70% e especificar quais são as task (US) e remover os "70%" (Meter as task que tiverem com a label must have); ✅
- Se possivel não falar com percentagens, mas sim com (US) (TASK´S); ✅
- Ser mais concreto nos riscos; ✅
- Rever os riscos, a falar de coisas que sejam de verdade no momento (remover o risco 6, 7, porque não começamos a desenvolver nada ainda); ✅
- Rescrever o Mitigation Action; ✅
- Remover as tasks da sprint 0 do close backlog; ✅
- Mover as US que são para fazer para os in progress backlog; ✅
- Faltam os criterios de aceitação para a sprint 1;
- Adicionar os criterios de aceitação no sprint backlog;
- Fazer uma tabela com todos os utilizadores e vem como as tarefas que cada um pode desenvolver; ✅
- Clarificar sprint 1; ✅
- Clarificar US; ✅
- Substituir os nomes das labels; ✅
- US no backlog; ✅
- Criar os cartoes de criterios de aceitação; ✅
- Usar o método do planning pocker para estimar as US/TASK´S; (FAZER REUNIÃO PARA ESTIMAR AS US/TASK´S); ✅

## Reunião 4 - 02/11/2023 ás 14h40:
- Cartão para o login (US, Criterios de Aceitação); ✅
- Fazer os testes para o login e para as restantes task´s (criar/editar/eliminar); ✅
- Criar uma labela para cada sprint e associar a cada US; ✅
- Meter a tabela com as tarefas do excel criada no readme; ✅
- Criar testes unitários para todas as task´s;
- No final de cada US fazer teste de aceitação;
- Criar a base de dados; ✅

## Reunião 5 - 09/11/2023 ás 15h50
- Inserir utilizadores/animais/voluntarios = Unit Test´s;
- Testes de aceitação para as interfaces (botões, etc) feitos à mão;
- Fechar só os testes de aceitação quando as US tiverem completas ou seja close;
- Associar os criterios de aceitação as milestone; ✅
- Adicionar tempo aos criterios de aceitação;
- Meter as labels dos criterios de aceitação corretas; ✅
- Preparar a proposta para a proxima sprint (US, TASK´S, HORAS)


***

## Tópicos Reuniões
### Reunião 19/10:
- Atualizamos os critérios de aceitação (given/when/then);
- Adicionamos Mokups;
- Atualizamos a Risk List;
- Criamos uma issue para cada US;
- Criamos as várias boards para cada sprint;

### Reunião 26/10:
- Mockups completos com o novo modelo pedido - agora já existe uma lista extensa dos animais ou voluntários com uma caixa de pesquisa, também existe um campo para a saúde do animal e espaço para comentários;
- Critérios de aceitação das US resolvidos - está na maneira correta do given/when/then;
- Reestruturação do risk plan: threshhold ajustado, risk list escrita da maneira correta, mitigation actions corrijidas;
- Datas dos 2 release plans corrijidas, objetivos definidos;
- Sprint backlog da sprint 1 completo;
- Product backlog colocado no sítio certo (Plan > Issue Boards);

### Reunião 02/11:
- ReadMe atualizado;
- Issues atualizados;
- Boards atualizadas:
    - Backlog;
    - InProgressPB;
    - ClosedPB;
- Codigo adicionado;

### Reunião 09/11:
- Adicionados Issues para os Criterios de Aceitação;
- Base de dados adicionada;
- Codigo atualizado;

### Reunião 16/11:
- Código atualizado;
- Conclusão da Sprint1;

### Reunião 23/11:
- Correção de bugs;
- Efetuadas as alterações sugeridas pelo professor(Linguagem usada, termos, etc);
- Conclusão da Sprint 1 Review;
- Atualização das US/Tasks´s da sprint 2;
- Planning Poker efetuado para as task´s da sprint 2;
- Iniciação da implementação de algumas US;

### Reunião 30/11
- Correção de bugs;
- Efetuadas as US´s em falta;
- Finalização da Sprint 2 Review;
- Elaboração da US´s da sprint, bem como dos seus criterios de aceitação e tasks;
- Planning Poker efetuado para as task´s da sprint 3;
